package im.djm.block.nulls;

import im.djm.block.Hash;

/**
 * 
 * @author djm.im
 *
 */
public final class NullHash extends Hash {

	/**
	 * 
	 */
	public NullHash() {
		super(new byte[32]);
	}

}
